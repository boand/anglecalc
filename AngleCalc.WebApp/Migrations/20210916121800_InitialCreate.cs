﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AngleCalc.WebApp.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CameraOptions",
                columns: table => new
                {
                    RequestDateTime = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "SYSDATETIME()"),
                    DistanceToObject = table.Column<int>(type: "int", nullable: false),
                    HeightFromFloor = table.Column<int>(type: "int", nullable: false),
                    HeightFromObject = table.Column<int>(type: "int", nullable: false),
                    AngleAlpha = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CameraOptions", x => x.RequestDateTime);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CameraOptions");
        }
    }
}
