﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AngleCalc.WebApp.Migrations
{
    public partial class Added_Title : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_CameraOptions",
                table: "CameraOptions");

            migrationBuilder.DropColumn(
                name: "RequestDateTime",
                table: "CameraOptions");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "CameraOptions",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "CameraOptions",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "old");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CameraOptions",
                table: "CameraOptions",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_CameraOptions",
                table: "CameraOptions");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "CameraOptions");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "CameraOptions");

            migrationBuilder.AddColumn<DateTime>(
                name: "RequestDateTime",
                table: "CameraOptions",
                type: "datetime2",
                nullable: false,
                defaultValueSql: "SYSDATETIME()");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CameraOptions",
                table: "CameraOptions",
                column: "RequestDateTime");
        }
    }
}
