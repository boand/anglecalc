﻿export class CameraModel {
    constructor(
        public name?: string,
        public a?: number,
        public d?: number) { }
}