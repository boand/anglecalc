﻿import { Component, OnInit } from "@angular/core";
import { DataService } from "./data.service";
import { SearchOptionsModel } from "./searchOptionsModel";
import { CameraOptions } from "./cameraOptions";
import { CameraModel } from "./cameraModel";
import { SearchModel } from "./SearchModel";

@Component({
    selector: "app",
    templateUrl: "./app.component.html",
    providers: [DataService]
})
export class AppComponent implements OnInit {
    searchModel = new SearchModel();
    cameraModel = new CameraModel();
    cameraOptions: CameraOptions[];
    tableMode = true;
    hasPreviousPage = false;
    hasNextPage = false;

    constructor(private dataService: DataService) {}

    ngOnInit() {
        this.loadOptions();
    }

    loadOptions() {
        this.dataService.getOptions(this.searchModel)
            .subscribe((data: SearchOptionsModel) => {
                this.hasPreviousPage = data.pageViewModel.hasPreviousPage;
                this.hasNextPage = data.pageViewModel.hasNextPage;
                return this.cameraOptions = data.cameraOptions;
            });
    }

    calculate() {
        this.dataService.calculateAlpha(this.cameraModel)
            .subscribe((data: CameraOptions) => this.cameraOptions.push(data));
        this.cancel();
    }

    cancel() {
        this.cameraModel = new CameraModel();
        this.tableMode = true;
    }

    delete(o: CameraOptions) {
        this.dataService.deleteOptions(o.id)
            .subscribe(_ => this.loadOptions());
    }

    add() {
        this.cancel();
        this.tableMode = false;
    }
}