﻿import { CameraOptions } from './cameraOptions';
import { PageViewModel } from "./PageViewModel";

export class SearchOptionsModel {
    constructor(
        public pageViewModel: PageViewModel,
        public cameraOptions: CameraOptions[]) { }
}