﻿import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { CameraModel } from "./cameraModel";
import { SearchModel } from "./SearchModel";

@Injectable()
export class DataService {

    private url = "api/camera/calculateAlpha";

    constructor(private http: HttpClient) {
    }

    getOptions(model: SearchModel) {
        let params = new HttpParams();
        if (model.id != null) params = params.append("id", (model.id) as any);
        if (model.name != null) params = params.append("name", model.name);

        return this.http.get(this.url, { params: params });
    }

    calculateAlpha(model: CameraModel) {
        return this.http.post(this.url, model);
    }

    deleteOptions(id: number) {
        return this.http.delete(this.url + "/" + id);
    }
}