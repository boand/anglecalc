﻿export class CameraOptions {
    constructor(
        public id: number,
        public title: string,
        public distanceToObject: number,
        public heightFromFloor: number,
        public heightFromObject: number,
        public angleAlpha: number) { }
}