﻿export class PageViewModel {
    constructor(
        public hasPreviousPage: boolean,
        public hasNextPage: boolean) { }
}