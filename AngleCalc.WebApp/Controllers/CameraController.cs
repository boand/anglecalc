﻿using System.Linq;
using System.Threading.Tasks;
using AngleCalc.Infrastructure.Models;
using AngleCalc.Infrastructure.Services.Interfaces;
using AngleCalc.WebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AngleCalc.WebApp.Controllers
{
    /// <summary>Камера</summary>
    [ApiController]
    [Route("api/[controller]")]
    public class CameraController : ControllerBase
    {
        private readonly ICameraService _cameraService;
        private readonly ApplicationContext _context;

        public CameraController(ApplicationContext context, ICameraService cameraService)
        {
            _context = context;
            _cameraService = cameraService;
        }

        /// <summary>Отображение результатов расчета.</summary>
        [HttpGet("CalculateAlpha")]
        public async Task<SearchOptionsModel> SearchOptions([FromQuery] SearchModel model)
        {
            var query = _context.CameraOptions
                .Where(x => x.Id == (model.Id ?? x.Id))
                .Where(x => x.Title == (model.Name ?? x.Title));
            var count = await query.CountAsync();
            var items = await query
                .Skip((model.PageNumber - 1) * model.PageSize)
                .Take(model.PageSize)
                .ToArrayAsync();

            return new SearchOptionsModel
            {
                PageViewModel = new PageViewModel(count, model.PageNumber, model.PageSize),
                CameraOptions = items
            };
        }

        /// <summary>Рассчитать вертикальный угол наклона камеры.</summary>
        /// <param name="cameraModel">Параметры для расчета.</param>
        /// <returns>Вертикальный угол наклона камеры.</returns>
        [HttpPost("CalculateAlpha")]
        public async Task<CameraOptions> CalculateAlpha(CameraModel cameraModel)
        {
            var cameraOptions = _cameraService.CalculateOptions(cameraModel.Name, cameraModel.D, cameraModel.A);

            _context.CameraOptions.Add(cameraOptions);
            await _context.SaveChangesAsync();

            return cameraOptions;
        }

        /// <summary>Удаление результатов расчета.</summary>
        [HttpDelete("CalculateAlpha/{id}")]
        public async Task<IActionResult> DeleteOptions(int id)
        {
            var options = await _context.CameraOptions.FindAsync(id);
            if (options == null)
                return NotFound();

            _context.CameraOptions.Remove(options);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}