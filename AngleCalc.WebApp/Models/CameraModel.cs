﻿using System.ComponentModel.DataAnnotations;

namespace AngleCalc.WebApp.Models
{
    /// <summary>Параметры камеры.</summary>
    public class CameraModel
    {
        /// <summary>Расстояние от камеры до объекта в см.</summary>
        public int A { get; set; }

        /// <summary>Высота установки камеры от пола в см.</summary>
        public int D { get; set; }

        /// <summary>Наименование расчёта.</summary>
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
    }
}