﻿namespace AngleCalc.WebApp.Models
{
    public class SearchModel : PageparamViewModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}