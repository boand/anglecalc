﻿using System.Collections.Generic;
using AngleCalc.Infrastructure.Models;

namespace AngleCalc.WebApp.Models
{
    public class SearchOptionsModel
    {
        public PageViewModel PageViewModel { get; set; }
        public IEnumerable<CameraOptions> CameraOptions { get; set; }
    }
}