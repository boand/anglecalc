﻿namespace AngleCalc.WebApp.Models
{
    public class PageparamViewModel
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }
}