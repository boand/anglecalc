﻿using AngleCalc.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;

namespace AngleCalc.WebApp.Models
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<CameraOptions> CameraOptions { get; set; }
    }
}