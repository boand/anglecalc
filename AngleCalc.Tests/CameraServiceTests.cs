using AngleCalc.Infrastructure.Services;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AngleCalc.Tests
{
    [TestClass]
    public class CameraServiceTests
    {
        private readonly CameraService _cameraService;

        public CameraServiceTests()
        {
            _cameraService = new CameraService(Options.Create(new CameraServiceOptions
                {HeightObject = 160, RoundAccuracy = 2}));
        }

        [TestMethod]
        public void CalculateTest()
        {
            var calculateOptions = _cameraService.CalculateOptions("test", 350, 700);

            Assert.AreEqual(190, calculateOptions.HeightFromObject);
            Assert.AreEqual(15.19, calculateOptions.AngleAlpha);
        }

        [TestMethod]
        public void CalculateZeroDivisionTest()
        {
            var calculateOptions = _cameraService.CalculateOptions("test", 350, 0);

            Assert.AreEqual(190, calculateOptions.HeightFromObject);
            Assert.AreEqual(0, calculateOptions.AngleAlpha);
        }
    }
}