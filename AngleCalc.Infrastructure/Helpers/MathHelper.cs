﻿using System;

namespace AngleCalc.Infrastructure.Helpers
{
    internal static class MathHelper
    {
        /// <summary>Развернутый угол.</summary>
        private const int Unfolded = 180;

        /// <summary>Преобразовать радианы в градусы.</summary>
        /// <param name="radians">радианы</param>
        /// <returns>градусы</returns>
        public static double RadiansToDegrees(double radians)
        {
            return radians * (Unfolded / Math.PI);
        }

        /// <summary>Преобразовать градусы в радианы.</summary>
        /// <param name="degrees">градусы</param>
        /// <returns>радианы</returns>
        public static double DegreesToRadians(double degrees)
        {
            return degrees * (Math.PI / Unfolded);
        }
    }
}