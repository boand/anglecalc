﻿using AngleCalc.Infrastructure.Services;
using AngleCalc.Infrastructure.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AngleCalc.Infrastructure
{
    public static class DependencyGroup
    {
        public static IServiceCollection AddInfrastructureDependencyGroup(this IServiceCollection services,
            IConfiguration config)
        {
            services.Configure<CameraServiceOptions>(config.GetSection(CameraServiceOptions.CameraService));
            services.AddScoped<ICameraService, CameraService>();
            return services;
        }
    }
}