﻿using System.ComponentModel.DataAnnotations;

namespace AngleCalc.Infrastructure.Models
{
    /// <summary>Параметры камеры.</summary>
    public class CameraOptions
    {
        /// <summary>Идентификатор.</summary>
        [Key]
        public int Id { get; set; }

        /// <summary>Наименование.</summary>
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }

        /// <summary>Расстояние от камеры до объекта в см. (A)</summary>
        public int DistanceToObject { get; set; }

        /// <summary>Высота установки камеры от пола в см. (D)</summary>
        public int HeightFromFloor { get; set; }

        /// <summary>Высота установки камеры от объекта в см. (B)</summary>
        public int HeightFromObject { get; set; }

        /// <summary>Вертикальный угол наклона камеры.</summary>
        [Range(-90, 90)]
        public double AngleAlpha { get; set; }
    }
}