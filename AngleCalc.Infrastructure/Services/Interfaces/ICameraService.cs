﻿using AngleCalc.Infrastructure.Models;

namespace AngleCalc.Infrastructure.Services.Interfaces
{
    public interface ICameraService
    {
        CameraOptions CalculateOptions(string title, int heightFromFloor, int distanceToObject);
    }
}