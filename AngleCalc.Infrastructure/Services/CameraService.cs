﻿using System;
using AngleCalc.Infrastructure.Helpers;
using AngleCalc.Infrastructure.Models;
using AngleCalc.Infrastructure.Services.Interfaces;
using Microsoft.Extensions.Options;

namespace AngleCalc.Infrastructure.Services
{
    public class CameraService : ICameraService
    {
        private readonly CameraServiceOptions _options;

        public CameraService(IOptions<CameraServiceOptions> options)
        {
            _options = options.Value;
        }

        public CameraOptions CalculateOptions(string title, int heightFromFloor, int distanceToObject)
        {
            var cameraOptions = new CameraOptions
            {
                Title = title,
                HeightFromFloor = heightFromFloor,
                DistanceToObject = distanceToObject
            };

            CalculateHeightFromObject(cameraOptions);
            CalculateAlpha(cameraOptions);

            return cameraOptions;
        }

        private void CalculateHeightFromObject(CameraOptions cameraOptions)
        {
            cameraOptions.HeightFromObject = cameraOptions.HeightFromFloor - _options.HeightObject;
        }

        private void CalculateAlpha(CameraOptions cameraOptions)
        {
            if (cameraOptions.DistanceToObject == 0)
            {
                //TODO Утотчнить корректно ли утверждение, что если объект стоит вплотную к камере то угол считаем за 0.
                cameraOptions.AngleAlpha = 0;
                return;
            }

            var tan = (double) cameraOptions.HeightFromObject / cameraOptions.DistanceToObject;
            var radians = Math.Atan(tan);
            var degrees = MathHelper.RadiansToDegrees(radians);
            cameraOptions.AngleAlpha = Math.Round(degrees, _options.RoundAccuracy);
        }
    }
}