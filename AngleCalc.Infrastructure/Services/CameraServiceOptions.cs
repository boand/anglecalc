﻿namespace AngleCalc.Infrastructure.Services
{
    public class CameraServiceOptions
    {
        public const string CameraService = "CameraService";

        public int HeightObject { get; set; }
        public int RoundAccuracy { get; set; }
    }
}